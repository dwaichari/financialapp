<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';

    /**
     * Get the post the name of a certain payment.
     */
    public function client()
    {
        return $this->belongsTo('App\Client','client_id');
    }
}
