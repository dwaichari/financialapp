<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SetPayment extends Model
{
    protected $table = 'set_payments';

    public function client()
    {
        return $this->belongsTo('App\Client');
    }
}
