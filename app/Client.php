<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public function payments()
    {
        return $this->hasMany('App\Payment','client_id');
    }
    public function setpayments()
    {
        return $this->hasMany('App\SetPayment','client_id');
    }

    // this is a recommended way to declare event handlers
    protected static function boot() {
        parent::boot();
        static::deleting(function($client) { // before delete() method call this
            foreach($client->payments as $payment)
            $payment->delete();

        });

        // the following code is for deleting the set payments for a client whenever he/she is deleted

        static::deleting(function($client) { // before delete() method call this
            foreach($client->setpayments as $setpayment)
                $setpayment->delete();

        });
    }
//inverse of the relationship between the sponsor and the client
    public function sponsor()
    {
        return $this->belongsTo('App\Sponsor');
    }

    //inverse of the relationship between the sponsor and the station
    public function station()
    {
        return $this->belongsTo('App\Station');
    }

    //function to capitalize the first letter of the names (mutator)
    public function setFirstNameAttribute($value) {
        $this->attributes['firstname'] = ucfirst($value);
    }
    public function setLastNameAttribute($value) {
        $this->attributes['lastname'] = ucfirst($value);
    }
}
