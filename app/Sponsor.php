<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
    protected $table = 'sponsors';

    //relationship between the client and the sponsor
    public function clients()
    {
        return $this->hasMany('App\Client','sponsor_id');
    }

    //function to capitalize the first letter of the names (mutator)
    public function setFirstNameAttribute($value) {
        $this->attributes['firstname'] = ucfirst($value);
    }
    public function setLastNameAttribute($value) {
        $this->attributes['lastname'] = ucfirst($value);
    }
}
