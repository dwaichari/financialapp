<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    protected $table = 'stations';

    //relationship between the client and the station
    public function clients()
    {
        return $this->hasMany('App\Client','station_id');
    }

    //function to capitalize the first letter of the station Name (mutator)
    public function setFirstNameAttribute($value) {
        $this->attributes['name'] = ucfirst($value);
    }
}
