<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ClearanceMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (Auth::user()->hasPermissionTo('Administer roles & permissions')) //If user has this //permission
        {
            return $next($request);
        }

        // start of Clients Controller

        /*if($request->is(route('clients.index')))//If user is viewing all the clients
        {
            if (!Auth::user()->hasPermissionTo('show-clients'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }*/
        /*if ($request->is(route('clients.create')))//If user is adding a new client
        {
            if (!Auth::user()->hasPermissionTo('add-client'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }
        if ($request->is(route('clients.store')))//If user is storing a new client
        {
            if (!Auth::user()->hasPermissionTo('add-client'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

        if ($request->is(route('clients.edit')))//If user is editing a  client
        {
            if (!Auth::user()->hasPermissionTo('edit-client'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }
        if ($request->is(route('clients.update')))//If user is editing a  client
        {
            if (!Auth::user()->hasPermissionTo('edit-client'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

        if ($request->is(route('clients.destroy')))//If user is deleting  a  client
        {
            if (!Auth::user()->hasPermissionTo('delete-client'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

        // start of Sponsors Controller

        if ($request->is(route('sponsors.index')))//If user is viewing all the sponsors
        {
            if (!Auth::user()->hasPermissionTo('show-sponsors'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }
        if ($request->is(route('sponsors.create')))//If user is adding a new sponsor
        {
            if (!Auth::user()->hasPermissionTo('add-sponsor'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }
        if ($request->is(route('sponsors.store')))//If user is storing a new sponsor
        {
            if (!Auth::user()->hasPermissionTo('add-sponsor'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

        if ($request->is(route('sponsors.edit')))//If user is editing a  sponsor
        {
            if (!Auth::user()->hasPermissionTo('edit-sponsor'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }
        if ($request->is(route('sponsors.update')))//If user is editing a  sponsor
        {
            if (!Auth::user()->hasPermissionTo('edit-sponsor'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

        if ($request->is(route('sponsors.destroy')))//If user is deleting  a  sponsor
        {
            if (!Auth::user()->hasPermissionTo('delete-sponsor'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

        // start of SetPayments Controller

        if ($request->is(route('setpayments.index')))//If user viewing all the set of payments
        {
            if (!Auth::user()->hasPermissionTo('show-setofpayments'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }
        if ($request->is(route('setpayments.create')))//If user is adding a new set of payments
        {
            if (!Auth::user()->hasPermissionTo('add-seofpayment'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

        if ($request->is(route('setpayments.store')))//If user is storing a new set of payments
        {
            if (!Auth::user()->hasPermissionTo('add-seofpayment'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

        if ($request->is(route('setpayments.edit')))//If user is editing a  set of payments
        {
            if (!Auth::user()->hasPermissionTo('edit-seofpayment'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }
        if ($request->is(route('setpayments.update')))//If user is editing a  set of payments
        {
            if (!Auth::user()->hasPermissionTo('edit-seofpayment'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

        if ($request->is(route('setpayments.destroy')))//If user is deleting  a  set of payments
        {
            if (!Auth::user()->hasPermissionTo('delete-seofpayment'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }
            // start of Payments Controller

        if ($request->is(route('payments.create')))//If user is adding a new payment
        {
            if (!Auth::user()->hasPermissionTo('add-payment'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }
        if ($request->is(route('payments.index')))//If user is viewing the payments
        {
            if (!Auth::user()->hasPermissionTo('show-payments'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

        if ($request->is(route('payments.store')))//If user is storing a new payment
        {
            if (!Auth::user()->hasPermissionTo('add-payment'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

        if ($request->is(route('payments.edit')))//If user is editing a  payment
        {
            if (!Auth::user()->hasPermissionTo('edit-payment'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }
        if ($request->is(route('payments.update')))//If user is editing a  payment
        {
            if (!Auth::user()->hasPermissionTo('edit-payment'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

        if ($request->is(route('payments.destroy')))//If user is deleting  a  payment
        {
            if (!Auth::user()->hasPermissionTo('delete-payment'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

// start of Stations Controller

        if ($request->is(route('stations.index')))//If user viewing all the stations
        {
            if (!Auth::user()->hasPermissionTo('show-stations'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }
        if ($request->is(route('stations.create')))//If user is adding a new station
        {
            if (!Auth::user()->hasPermissionTo('add-station'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }
        if ($request->is(route('stations.store')))//If user is storing a new station
        {
            if (!Auth::user()->hasPermissionTo('add-station'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

        if ($request->is(route('stations.edit')))//If user is editing a  station
        {
            if (!Auth::user()->hasPermissionTo('edit-station'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }
        if ($request->is(route('stations.update')))//If user is editing a  station
        {
            if (!Auth::user()->hasPermissionTo('edit-station'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }

        if ($request->is(route('stations.destroy')))//If user is deleting  a  station
        {
            if (!Auth::user()->hasPermissionTo('delete-payment'))
            {
                abort('401');
            }
            else {
                return $next($request);
            }
        }*/
    }
}