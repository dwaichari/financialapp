<?php

namespace App\Http\Controllers;

use App\Station;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StationsController extends Controller
{
    public function __construct() {
        $this->middleware('auth'); //you have to be looged in to access this controller
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->can('show-stations'))
        {
            $stations = Station::all();
            return view('stations.index',compact('stations'));
        }
        abort('401');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->can('add-station'))
        {
            return view('stations.create');
        }
        abort('401');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->can('add-station'))
        {
            //Validate data
            $this->validate($request, [
                'name'=>'required',
                'location'=>'required',

            ]);
            $addStation = new Station();
            $addStation->name = $request->name;
            $addStation->location = $request->location;
            $addStation->save();

            return redirect(route('stations.index'))->with('message','A new station has been added');
        }
        abort('401');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Auth::user()->can('show-station'))
        {
            $stationDetails = Station::find($id);
            return view('stations.show',compact('stationDetails'));
        }
        abort('401');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->can('edit-station'))
        {
            $editStation = Station::find($id);
            return view('stations.edit',compact('editStation'));
        }
        abort('401');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->can('edit-station'))
        {
            //Validate data
            $this->validate($request, [
                'name'=>'required',
                'location'=>'required',

            ]);
            $updateStation = Station::find($id);
            $updateStation->name = $request->name;
            $updateStation->location = $request->location;
            $updateStation->save();

            return redirect(route('stations.index'))->with('message','Station details updated successfully');
        }
        abort('401');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::user()->can('edit-station'))
        {
            $deleteStation = Station::find($id);
            $deleteStation->delete();

            return redirect(route('stations.index'))->with('message','Station deleted successfully');
        }
        abort('401');

    }
}
