<?php

namespace App\Http\Controllers;
use App\Client;
use Illuminate\Http\Request;
use App\Payment;
use Illuminate\Support\Facades\Auth;

class PaymentsController extends Controller
{
    public function __construct() {
        $this->middleware('auth'); //you have to be looged in to access this controller
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->can('show-payments'))
        {
            $payments = Payment::all();
            return view('payments.index',compact('payments'));
        }
        abort('401');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->can('add-payment'))
        {
            $clients = Client::all();
            return view('payments.create',compact('clients'));
        }
        abort('401');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->can('add-payment'))
        {
            //validate data
            $this->validate($request, [
                'client_id'=>'required',
                'tuitionfee'=>'numeric',
                'nursefee'=>'numeric',
                'psychiatristfee'=>'numeric',
                'medicalfee'=>'numeric',
                'screeningfee'=>'numeric',

            ]);
            $pay = new Payment();
            $pay->client_id = $request->client_id;
            $pay->tuitionfee = $request->tuitionfee;
            $pay->nursefee = $request->nursefee;
            $pay->psychiatristfee = $request->psychiatristfee;
            $pay->medicalfee = $request->medicalfee;
            $pay->screeningfee = $request->screeningfee;

            $pay->save();

            return redirect(route('payments.index'))->with('message','A new payment has been added');
        }
        abort('401');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Auth::user()->can('show-payment'))
        {
            $paymentDetails = Payment::find($id);
            return view('payments.show',compact('paymentDetails'));
        }
        abort('401');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->can('edit-payment'))
        {
            $editPayment = Payment::find($id);
            $clients = Client::all();
            return view('payments.edit',compact(['editPayment','clients']));
        }
        abort('401');


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->can('edit-payment'))
        {
            //validate data
            $this->validate($request, [
                'client_id'=>'required',
                'tuitionfee'=>'numeric',
                'nursefee'=>'numeric',
                'psychiatristfee'=>'numeric',
                'medicalfee'=>'numeric',
                'screeningfee'=>'numeric',

            ]);
            $updatePayment = Payment::find($id);
            $updatePayment->tuitionfee = $request ->tuitionfee;
            $updatePayment->nursefee = $request->nursefee;
            $updatePayment->psychiatristfee = $request->psychiatristfee;
            $updatePayment->medicalfee = $request->medicalfee;
            $updatePayment->screeningfee = $request->screeningfee;

            $updatePayment->save();

            return redirect(route('payments.index'))->with('message','Payment has been updated successfully');
        }
        abort('401');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::user()->can('delete-payment'))
        {
            $deletePayment = Payment::find($id);
            $deletePayment->delete();

            return redirect(route('payments.index'))->with('message','Payment Deleted Successfully');
        }
        abort('401');

    }
}
