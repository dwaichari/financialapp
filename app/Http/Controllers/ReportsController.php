<?php

namespace App\Http\Controllers;

use App\Client;
use App\Payment;
use App\SetPayment;
use App\Sponsor;
use App\Station;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class ReportsController extends Controller
{
    public function __construct() {
        $this->middleware('auth'); //you have to be looged in to access this controller
    }

    //the function below is for showing all the totals of all the set of payments for all the clients
    public function showclientssetofpayments()
    {
        $clientssetofpayments = Client::all();
        return view('reports.clientssetofpaymentstotals',compact('clientssetofpayments'));
    }

    //this function is showing all the balances in all the set payments for the clients

    public function balances()
    {
        $clients = Client::all();
        return view('reports.balances',compact('clients'));
    }

    //function for displaying the payment history of a client

    public function paymenthistory($id)
    {
        $paymenthistories = Client::find($id)->payments;
        $clientdetails = Client::find($id);
        return view('reports.clientspaymenthistory',compact(['paymenthistories','clientdetails']));
    }

    // this function shows all the clients who are in session i.e they have not yet cleared.

    public function active()
    {
        $clients = Client::all();

        foreach ($clients as $client)
        {
            if ($client->exitdate == '')
            {
                return $clients;
            }
            else
                {
                    return 'There are no active clients at this time';
                }
        }
    }

    //function to get all the clients in a given station

    public function stationClients($id)
    {
        $stationDetails = Station::find($id);
        $stationClients = Station::find($id)->clients;

        return view('reports.stationclients',compact(['stationDetails','stationClients']));

    }

    //function for retriving the clients of a specific sponsor

    public function sponsorClients($id)
    {
        $sponsorDetails = Sponsor::find($id);
        $sponsorClients = Sponsor::find($id)->clients;

        return view('reports.sponsorclients',compact(['sponsorClients','sponsorDetails']));
    }

    //function for showing total amount of money that the client has paid in Total per category

    public function paymentTotals()
    {
        $clients = Client::all();
        return view('reports.paymentstotal',compact('clients'));
    }

    //function to retrive all the clients that have registered today
    public function clientsregisteredtoday()
    {
        $currentdate =Carbon::today()->toDateString();
        $todaysclients =Client::whereDate('created_at', DB::raw('CURDATE()'))->get();
        return view('reports.clientsregisteredtoday',compact(['todaysclients','currentdate']));
    }

    //function to retrive all the sponsors that have registered today
    public function sponsorsregisteredtoday()
    {
        $currentdate =Carbon::today()->toDateString();
        $todayssponsors =Sponsor::whereDate('created_at', DB::raw('CURDATE()'))->get();
        return view('reports.sponsorsregisteredtoday',compact(['todayssponsors','currentdate']));
    }

    //function to get all the clients set of payments history

    public function clientssetofpaymentshistory($id)
    {
        $client = Client::find($id);
        $setofpaymenthistories = Client::find($id)->setpayments;
        return view('reports.clientssetofpaymentshistory',compact(['client','setofpaymenthistories']));
    }

    //function for showing all the payments made today
    public function paymentsfortoday()
    {
        $currentdate =Carbon::today()->toDateString();
        $todayspayments = Payment::whereDate('created_at', DB::raw('CURDATE()'))->get();
        return view('reports.todayspayments',compact(['todayspayments','currentdate']));
    }

    //function for showing all the set of payments made today
    public function todayssetofpayments()
    {
        $currentdate =Carbon::today()->toDateString();
        $todayssetpayments = SetPayment::whereDate('created_at', DB::raw('CURDATE()'))->get();
        return view('reports.todayssetofpayments',compact(['todayssetpayments','currentdate']));
    }

    //function for showing each station financial information
    public function stationbalances($id)
    {
        $clients = Station::find($id)->clients;
        $station = Station::find($id);
       return view('reports.stationbalances',compact(['clients','station']));
    }

    //function for getting the payments made in a station

    public function stationpayments($id)
    {
        $station = Station::find($id);
        $clients = Station::find($id)->clients;
        return view('reports.stationpayments',compact(['station','clients']));

    }
}
