<?php

namespace App\Http\Controllers;

use App\Client;
use App\Payment;
use App\SetPayment;
use App\Sponsor;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
   //restrict acess to unregistered users
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard()
    {
        //code for getting all the number of clients
        $noofclients = count(Client::all());

        //code for counting all the clients registered today
        $todaysclients = count(Client::whereDate('created_at', DB::raw('CURDATE()'))->get());

        //code for getting all the number of sponsors
        $noofsponsors = count(Sponsor::all());

        //code for counting all the sponsors registered today
        $todaysponsors = count(Sponsor::whereDate('created_at', DB::raw('CURDATE()'))->get());

        //code for getting all total amount of payments for the day
        $payments = Payment::whereDate('created_at', DB::raw('CURDATE()'))->get();
        $totalfortodayspayments =  collect($payments)->sum('tuitionfee')
            +collect($payments)->sum('screeningfee')
            +collect($payments)->sum('nursefee')
            +collect($payments)->sum('psychiatristfee')
            +collect($payments)->sum('medicalfee');

        //code for displaying all the balance amount
        $setpaymentsbalance = SetPayment::all();
        $setpaymentsbalancetotal=  collect($setpaymentsbalance)->sum('tuitionfee')
            +collect($setpaymentsbalance)->sum('screeningfee')
            +collect($setpaymentsbalance)->sum('nursefee')
            +collect($setpaymentsbalance)->sum('psychiatristfee')
            +collect($setpaymentsbalance)->sum('medicalfee');

        $paymentsbalance = Payment::all();
        $paymentsbalancetotal=  collect($paymentsbalance)->sum('tuitionfee')
            +collect($paymentsbalance)->sum('screeningfee')
            +collect($paymentsbalance)->sum('nursefee')
            +collect($paymentsbalance)->sum('psychiatristfee')
            +collect($paymentsbalance)->sum('medicalfee');
        $balance = $setpaymentsbalancetotal - $paymentsbalancetotal;


        // code for showing all the set of payments made today
        $setofpaymentstoday = SetPayment::whereDate('created_at', DB::raw('CURDATE()'))->get();
        $setofpaymentstodaytotal=  collect($setofpaymentstoday)->sum('tuitionfee')
            +collect($setofpaymentstoday)->sum('screeningfee')
            +collect($setofpaymentstoday)->sum('nursefee')
            +collect($setofpaymentstoday)->sum('psychiatristfee')
            +collect($setofpaymentstoday)->sum('medicalfee');

        //code for showing all the total set of payments
        $setofpayments = SetPayment::all();
        $setofpaymentstotal=  collect($setofpayments)->sum('tuitionfee')
            +collect($setofpayments)->sum('screeningfee')
            +collect($setofpayments)->sum('nursefee')
            +collect($setofpayments)->sum('psychiatristfee')
            +collect($setofpayments)->sum('medicalfee');

        //code to get the latest payments
        $payments = Payment::all();;
        return view('dashboard.dashboard',compact(['noofclients',
            'todaysclients','todaysponsors','noofsponsors','totalfortodayspayments','balance',
            'setofpaymentstodaytotal','setofpaymentstotal','payments']));
    }

}
