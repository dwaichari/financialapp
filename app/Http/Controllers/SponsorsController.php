<?php

namespace App\Http\Controllers;

use App\Sponsor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SponsorsController extends Controller
{
    public function __construct() {
        $this->middleware('auth'); //you have to be looged in to access this controller
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->can('show-sponsors'))
        {
            $sponsors = Sponsor::all();
            return view('sponsors.index',compact('sponsors'));
        }
        abort('401');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->can('add-sponsor'))
        {
            return view('sponsors.create');
        }
        abort('401');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::user()->can('add-sponsor'))
        {
            //Validate data
            $this->validate($request, [
                'firstname'=>'required|max:120|alpha',
                'lastname'=>'required|max:120|alpha',
                'idnumber'=>'required|max:8',
                'gender'=>'required',
                'residence'=>'required|max:120',
                'contact'=>'required|max:15',

            ]);
            $addSponsor = new Sponsor();
            $addSponsor->firstname = $request->firstname;
            $addSponsor->lastname = $request->lastname;
            $addSponsor->idnumber = $request->idnumber;
            $addSponsor->gender = $request->gender;
            $addSponsor->contact = $request->contact;
            $addSponsor->residence = $request->residence;
            $addSponsor->occupation = $request->occupation;

            $addSponsor->save();

            return redirect(route('sponsors.index'))->with('message','A new sponsor has been added');
        }
        abort('401');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Auth::user()->can('show-sponsor'))
        {
            $sponsorDetails = Sponsor::find($id);

            return view('sponsors.show',compact('sponsorDetails'));
        }
        abort('401');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->can('edit-sponsor'))
        {
            $sponsorDetails = Sponsor::find($id);

            $editSponsor = Sponsor::find($id);

            return view('sponsors.edit',compact('editSponsor'));
        }
        abort('401');


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->can('edit-sponsor'))
        {
            //Validate data
            $this->validate($request, [
                'firstname'=>'required|max:120|alpha',
                'lastname'=>'required|max:120|alpha',
                'idnumber'=>'required|max:8',
                'gender'=>'required',
                'residence'=>'required|max:120',
                'contact'=>'required|max:15',

            ]);
            $updateSponsor = Sponsor::find($id);
            $updateSponsor->firstname = $request->firstname;
            $updateSponsor->lastname = $request->lastname;
            $updateSponsor->idnumber = $request->idnumber;
            $updateSponsor->gender = $request->gender;
            $updateSponsor->contact = $request->contact;
            $updateSponsor->residence = $request->residence;
            $updateSponsor->occupation = $request->occupation;

            $updateSponsor->save();
            return redirect(route('sponsors.index'))->with('message','Sponsor details has been updated successfully');
        }
        abort('401');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::user()->can('delete-sponsor'))
        {
            $deleteSponsor = Sponsor::find($id);
            $deleteSponsor->delete();
            return redirect(route('sponsors.index'))->with('message','Sponsor Deleted Successfully');
        }
        abort('401');

    }
}
