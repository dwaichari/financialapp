<?php

namespace App\Http\Controllers;

use App\Client;
use App\SetPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SetPaymentsController extends Controller
{
    public function __construct() {
        $this->middleware('auth'); //you have to be looged in to access this controller
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->can('show-setofpayments'))
        {
            $setpayments = SetPayment::all();
            return view('setpayments.index',compact('setpayments'));
        }
        abort('401');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->can('add-setofpayment'))
        {
            $clients = Client::all();
            return view('setpayments.create',compact('clients'));
        }
        abort('401');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (Auth::user()->can('add-setofpayment'))
        {
            //validate data
            $this->validate($request, [
                'client_id'=>'required',
                'tuitionfee'=>'numeric',
                'nursefee'=>'numeric',
                'psychiatristfee'=>'numeric',
                'medicalfee'=>'numeric',
                'screeningfee'=>'numeric',

            ]);
            $addSetPayment = new SetPayment();
            $addSetPayment->client_id = $request->client_id;
            $addSetPayment->tuitionfee = $request->tuitionfee;
            $addSetPayment->nursefee = $request->nursefee;
            $addSetPayment->psychiatristfee = $request->psychiatristfee;
            $addSetPayment->medicalfee = $request->medicalfee;
            $addSetPayment->screeningfee = $request->screeningfee;

            $addSetPayment->save();

            return redirect(route('setpayments.index'))->with('message','A new set of payment has been added');
        }
        abort('401');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->can('edit-setofpayment'))
        {
            $editsetpayment = SetPayment::find($id);

            return view('setpayments.edit',compact('editsetpayment'));

        }
        abort('401');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->can('edit-setofpayment'))
        {
            //validate data
            $this->validate($request, [
                'client_id'=>'required',
                'tuitionfee'=>'numeric',
                'nursefee'=>'numeric',
                'psychiatristfee'=>'numeric',
                'medicalfee'=>'numeric',
                'screeningfee'=>'numeric',

            ]);
            $updatesetpayment = SetPayment::find($id);
            $updatesetpayment->client_id = $request->client_id;
            $updatesetpayment->tuitionfee = $request->tuitionfee;
            $updatesetpayment->nursefee = $request->nursefee;
            $updatesetpayment->psychiatristfee = $request->psychiatristfee;
            $updatesetpayment->medicalfee = $request->medicalfee;
            $updatesetpayment->screeningfee = $request->screeningfee;

            $updatesetpayment->save();

            return redirect(route('setpayments.index'))->with('message','A set of payment has been updated successfully');
        }
        abort('401');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::user()->can('delete-setofpayment'))
        {
            $deletesetpayment = SetPayment::find($id);

            $deletesetpayment->delete();
            return redirect(route('setpayments.index'))->with('message','A set of payment has been deleted');

        }
        abort('401');

    }
}
