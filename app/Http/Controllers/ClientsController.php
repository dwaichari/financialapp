<?php

namespace App\Http\Controllers;

use App\Client;
use App\Sponsor;
use App\Station;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClientsController extends Controller
{
    public function __construct() {
        $this->middleware('auth'); //you have to be looged in to access this controller
    }


    //display all the clients from the database

    public function index()
    {
        if (Auth::user()->can('show-clients'))
        {
            $clients = Client::all();
            return view('clients.index',compact('clients'));
        }
        abort('401');

    }

    //loads the add clients page
    public function create()
    {
        if (Auth::user()->can('add-client'))
        {
            $sponsors = Sponsor::all();
            $stations = Station::all();
            return view('clients.create',compact(['sponsors','stations']));
        }
        abort('401');

    }

    //processes the submitted data and saves it to the database
    public function store(Request $request)
    {
        if (Auth::user()->can('add-client'))
        {
            //Validate data
            $this->validate($request, [
                'sponsor_id'=>'required',
                'station_id'=>'required',
                'firstname'=>'required|max:120|alpha',
                'lastname'=>'required|max:120|alpha',
                'dateofbirth'=>'required|max:120|date',
                'gender'=>'required|max:120|alpha',
                'residence'=>'required|max:120',
                'medicalhistory'=>'required',

            ]);
            $addclient = new Client();
            $addclient->sponsor_id = $request->sponsor_id;
            $addclient->station_id = $request->station_id;
            $addclient->firstname = $request->firstname;
            $addclient->lastname = $request->lastname;
            $addclient->idnumber = $request->idnumber;
            $addclient->dateofbirth = $request->dateofbirth;
            $addclient->gender = $request->gender;
            $addclient->contact = $request->contact;
            $addclient->residence = $request->residence;
            $addclient->medicalhistory = $request->medicalhistory;
            $addclient->expectedexitdate = $request->expectedexitdate;
            $addclient->exitdate = $request->exitdate;
            $addclient->exitcomments = $request->exitcomments;

            $addclient->save();
            return redirect(route('clients.index'))->with('message','Client Added Successfully');
        }
        abort('401');

    }

    /**
     * //Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Auth::user()->can('show-client'))
        {
            $clientDetails = Client::find($id);
            return view('clients.show',compact('clientDetails'));
        }
        abort('401');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()->can('edit-client'))
        {
            $sponsors = Sponsor::all();
            $stations = Station::all();
            $editDetails = Client::find($id);
            return view('clients.edit',compact(['editDetails','sponsors','stations']));
        }
        abort('401');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->can('edit-client'))
        {
            //Validate data
            $this->validate($request, [
                'sponsor_id'=>'required',
                'station_id'=>'required',
                'firstname'=>'required|max:120|alpha',
                'lastname'=>'required|max:120|alpha',
                'dateofbirth'=>'required|max:120|date',
                'gender'=>'required|max:120|alpha',
                'residence'=>'required|max:120',
                'medicalhistory'=>'required',

            ]);
            $updateClient = Client::find($id);
            $updateClient->sponsor_id = $request->sponsor_id;
            $updateClient->station_id = $request->station_id;
            $updateClient->firstname = $request->firstname;
            $updateClient->lastname = $request->lastname;
            $updateClient->idnumber = $request->idnumber;
            $updateClient->dateofbirth = $request->dateofbirth;
            $updateClient->gender = $request->gender;
            $updateClient->contact = $request->contact;
            $updateClient->residence = $request->residence;
            $updateClient->medicalhistory = $request->medicalhistory;
            $updateClient->expectedexitdate = $request->expectedexitdate;
            $updateClient->exitdate = $request->exitdate;
            $updateClient->exitcomments = $request->exitcomments;

            $updateClient->save();

            return redirect(route('clients.index'))->with('message','Client details updated successfully');
        }
        abort('401');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::user()->can('delete-client'))
        {
            $deleteClient = Client::find($id);
            $deleteClient->delete();
            return redirect(route('clients.index'))->with('message','Client deleted successfully ');
        }
        abort('401');

    }
}
