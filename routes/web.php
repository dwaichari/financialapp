<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','DashboardController@dashboard')->name('dashboard');
Route::get('/clients-registered-today','ReportsController@clientsregisteredtoday')->name('clientsregisteredtoday');
Route::get('/sponsors-registered-today','ReportsController@sponsorsregisteredtoday')->name('sponsorsregisteredtoday');
Route::get('/showlclientssetofpayments','ReportsController@showclientssetofpayments')->name('showclientssetofpayments');
Route::get('/client-set-of-payments-history/{id}','ReportsController@clientssetofpaymentshistory')->name('setofpaymentshistory');
Route::get('/clients-balances','ReportsController@balances')->name('balances');
Route::get('/client-payment-history/{id}','ReportsController@paymenthistory')->name('paymenthistory');
Route::resource('clients','ClientsController');
Route::resource('setpayments','SetPaymentsController');
Route::resource('payments','PaymentsController');
Route::resource('stations','StationsController');
Route::resource('sponsors','SponsorsController');
Route::get('/tuition-balance','BalancesController@getTuitionBalance');
Auth::routes();
Route::get('/active', 'ReportsController@active')->name('active');
Route::get('/station-clients/{id}', 'ReportsController@stationClients')->name('station.clients');
Route::get('/station-balances/{id}', 'ReportsController@stationbalances')->name('station.balances');
Route::get('/station-payments/{id}', 'ReportsController@stationpayments')->name('station.payments');
Route::get('/sponsor-clients/{id}', 'ReportsController@sponsorClients')->name('sponsor.clients');
Route::get('/payments-totals', 'ReportsController@paymentTotals')->name('payments.totals');
Route::get('/payments-made-today','ReportsController@paymentsfortoday')->name('paymentsfortoday');
Route::get('/set-of-payments-made-today','ReportsController@todayssetofpayments')->name('todayssetofpayments');
Route::resource('users', 'UsersController');
Route::resource('roles', 'RolesController');
Route::resource('permissions', 'PermissionsController');
