@extends('layouts.master')
@section('title')
    Serenity | Dashboard
@endsection
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        @if(auth()->user()->can('show-dashboard'))
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{!!$todaysclients !!}</h3>
                        <p>Clients Registered Today</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    @if(!$todaysclients == 0)
                    <a href="{!! route('clientsregisteredtoday') !!}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    @else
                        <a href="{!! route('clients.create') !!}" class="small-box-footer">Add a new client<i class="fa fa-arrow-circle-right"></i></a>
                    @endif
                </div>
            </div>
            <!-- ./col -->
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{!!$noofclients!!}</h3>

                        <p>Clients Across all Stations</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{{route('clients.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{$totalfortodayspayments}}<sup style="font-size: 20px"></sup></h3>
                        <p>Total paid today across all stations</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="{!! route('paymentsfortoday') !!}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>

            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$balance}}</h3>

                        <p>Total Balance for all the Clients</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="{{route('balances')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{!!$todaysponsors !!}</h3>
                        <p>Sponsors Registered Today</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    @if(!$todaysponsors == 0)
                        <a href="{!! route('sponsorsregisteredtoday') !!}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    @else
                        <a href="{!! route('sponsors.create') !!}" class="small-box-footer">Add a new Sponsor<i class="fa fa-arrow-circle-right"></i></a>
                    @endif

                </div>
            </div>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{!!$noofsponsors!!}</h3>

                        <p>Sponsors Across all Stations</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="{{route('sponsors.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{$setofpaymentstodaytotal}}<sup style="font-size: 20px"></sup></h3>
                        <p>Todays amount of set of payments </p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    @if(!$setofpaymentstodaytotal == 0)
                        <a href="{{route('todayssetofpayments')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                    @else
                        <a href="{!! route('setpayments.create') !!}" class="small-box-footer">Set a new payment<i class="fa fa-arrow-circle-right"></i></a>
                    @endif

                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{$setofpaymentstotal}}</h3>

                        <p>Set of Payments total for all</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12 col-xs-6">
                <!-- /.box -->
                <div class="box">
                    <div class="box-body">
                        <div class="box-header">
                            <h3 class="box-title">Payments from the latest</h3>
                        </div>
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th>Adm No</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Rehab </th>
                                <th>Nurse </th>
                                <th>Psychiatric </th>
                                <th>Medical</th>
                                <th>Screening</th>
                                <th>Date Paid</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payments as $payment)
                                <tr>
                                    <td class="text-center">{{ $loop->index + 1 }}</td>
                                    <td>{{str_replace(array('[',']','"'),'', $payment ->client()->pluck('id'))}}</td>
                                    <td><a href="{!! route('clients.show',str_replace(array('[',']','"'),'', $payment ->client()->pluck('id'))) !!}">{{str_replace(array('[',']','"'),'', $payment ->client()->pluck('firstname'))}}</a></td>
                                    <td><a href="{!! route('clients.show',str_replace(array('[',']','"'),'', $payment ->client()->pluck('id'))) !!}">{{str_replace(array('[',']','"'),'', $payment ->client()->pluck('lastname'))}}</a></td>
                                    <td>{!! $payment->tuitionfee !!}</td>
                                    <td>{!! $payment->nursefee !!}</td>
                                    <td>{!! $payment->psychiatristfee !!}</td>
                                    <td>{!! $payment->medicalfee !!}</td>
                                    <td>{!! $payment->screeningfee !!}</td>
                                    <td>{!! $payment->created_at !!}</td>
                                    <td><a href="{{ route('payments.edit',$payment->id) }}"><span class="glyphicon glyphicon-edit" style="margin-right: 8px;"></span></a>
                                        <a data-toggle="modal" data-target="#modal-danger-{{$payment->id}}"><span class="glyphicon glyphicon-trash "></span></a>
                                        <div class="modal modal-danger fade" id="modal-danger-{{$payment->id}}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Delete?</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure you want to permanently delete this set of payment?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal" onclick="event.preventDefault();">No</button>
                                                        <button type="button" class="btn btn-outline" onclick="
                                                                document.getElementById('delete-form-{{ $payment->id }}').submit();
                                                                ">Yes</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                            <form id="delete-form-{{ $payment->id }}" method="post"
                                                  action="{{ route('payments.destroy',$payment->id) }}" style="display: none">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                            </form>
                                        </div>
                                    </td>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th>Adm No</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Rehab </th>
                                <th>Nurse </th>
                                <th>Psychiatric </th>
                                <th>Medical</th>
                                <th>Screening</th>
                                <th>Date Paid</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
        <!-- /.modal -->

        <div class="modal modal-danger fade" id="modal-danger">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Danger Modal</h4>
                    </div>
                    <div class="modal-body">
                        <p>One fine body&hellip;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-outline">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
            @endif
    </section>
    <!-- /.content -->
@endsection