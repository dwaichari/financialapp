<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{!! asset('master/bower_components/bootstrap/dist/css/bootstrap.min.css')!!}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{!! asset('master/bower_components/font-awesome/css/font-awesome.min.css')!!}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{!! asset('master/bower_components/Ionicons/css/ionicons.min.css')!!}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{!! asset('master/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') !!}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{!! asset('master/dist/css/AdminLTE.min.css')!!}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{!! asset('master/dist/css/skins/_all-skins.min.css')!!}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{!! asset('master/bower_components/morris.js/morris.css')!!}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{!! asset('master/bower_components/jvectormap/jquery-jvectormap.css')!!}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{!! asset('master/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')!!}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{!! asset('master/bower_components/bootstrap-daterangepicker/daterangepicker.css')!!}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{!! asset('master/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')!!}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="{{route('dashboard')}}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>A</b>LT</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Serenity</b>Place</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{!! asset('master/dist/img/user2-160x160.jpg')!!}" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{!! asset('master/dist/img/user2-160x160.jpg')!!}" class="img-circle" alt="User Image">
                                <p>
                                    {{ Auth::user()->name }} - {{ Auth::user()->roles()->pluck('name')->implode(' ') }}
                                    <small>Member since {{ Auth::user()->created_at }}</small>
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="#" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-default btn-flat"
                                       href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    Sign out</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {!! csrf_field() !!}
                                    </form>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{!! asset('master/dist/img/user2-160x160.jpg')!!}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">MAIN NAVIGATION</li>
                <li class=" treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="{{route('dashboard')}}"><i class="fa fa-circle-o"></i> Dashboard</a></li>
                    </ul>
                </li>
                @if(auth()->user()->can('show-clients') || auth()->user()->can('add-client') )
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-users"></i>
                            <span>Clients</span>
                            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="{{route('clients.index')}}"><i class="fa fa-circle-o"></i> Clients List</a></li>
                            <li><a href="{{route('clients.create')}}"><i class="fa fa-circle-o"></i> Add Client</a></li>
                        </ul>
                    </li>
                @endif
                @if(auth()->user()->can('show-setofpayments') || auth()->user()->can('add-setofpayment') )
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-money"></i>
                        <span>Set Payments</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('setpayments.index')}}"><i class="fa fa-circle-o"></i> Set Payments List</a></li>
                        <li><a href="{{route('setpayments.create')}}"><i class="fa fa-circle-o"></i> Set a New Payment</a></li>
                    </ul>
                </li>
                @endif
                @if(auth()->user()->can('show-payments') || auth()->user()->can('add-payment') )
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-money"></i>
                        <span>Payments</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('payments.index')}}"><i class="fa fa-circle-o"></i> Payments List</a></li>
                        <li><a href="{{route('payments.create')}}"><i class="fa fa-circle-o"></i> Add Payment</a></li>
                    </ul>
                </li>
                @endif
                @if(auth()->user()->can('show-sponsors') || auth()->user()->can('add-sponsor') )
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i> <span>Sponsors</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{!! route('sponsors.index') !!}"><i class="fa fa-circle-o"></i> Sponsors List</a></li>
                        <li><a href="{!! route('sponsors.create') !!}"><i class="fa fa-circle-o"></i> Add Sponsor</a></li>
                    </ul>
                </li>
                @endif
                @if(auth()->user()->can('show-stations') || auth()->user()->can('add-station') )
                <li class="treeview">
                    <a href="#">
                        <i class="fa  fa-institution"></i> <span>Stations</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{!! route('stations.index')!!}"><i class="fa fa-circle-o"></i> Stations List</a></li>
                        @if(auth()->user()->can('add-station'))
                        <li><a href="{!! route('stations.create') !!}"><i class="fa fa-circle-o"></i> Add Station</a></li>
                            @endif
                    </ul>
                </li>
                @endif
                @if(auth()->user()->can('show-reports'))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder"></i> <span>Reports</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('showclientssetofpayments')}}"><i class="fa fa-circle-o"></i> Set of Payments Totals</a></li>
                        <li><a href="{{route('payments.totals')}}"><i class="fa fa-circle-o"></i> Payments Totals</a></li>
                        <li><a href="{{route('balances')}}"><i class="fa fa-circle-o"></i> Balances</a></li>
                    </ul>
                </li>
                @endif
                @if(auth()->user()->can('administer roles & permissions'))
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Users</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('users.index')}}"><i class="fa fa-circle-o"></i> Users List</a></li>
                        <li><a href="{{route('users.create')}}"><i class="fa fa-circle-o"></i> Add User</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa  fa-key"></i>
                        <span>Roles</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('roles.index')}}"><i class="fa  fa-circle-o"></i> Roles List</a></li>
                        <li><a href="{{route('roles.create')}}"><i class="fa  fa-circle-o"></i> Add Role</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa  fa-key"></i>
                        <span>Permissions</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('permissions.index')}}"><i class="fa  fa-circle-o"></i> Permissions List</a></li>
                        <li><a href="{{route('permissions.create')}}"><i class="fa  fa-circle-o"></i> Add Permission</a></li>
                    </ul>
                </li>
                    @endif
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @if(Session::has('message'))
            <p class="alert alert-info">{{ Session::get('message') }}</p>
        @endif
            @include('errors.errors')
       @section('main-content')
           @show
    </div>
    <!-- /.content-wrapper -->
    <footer class="main-footer">
        <strong>Copyright &copy; 2018-Date <a href="https:nawaisoft.co.ke">Nawaisoft Kenya</a>.</strong> All rights
        reserved.
    </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="{!! asset('master/bower_components/jquery/dist/jquery.min.js')!!}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{!! asset('master/bower_components/jquery-ui/jquery-ui.min.js')!!}"></script>
<!-- DataTables -->
<script src="{!! asset('master/bower_components/datatables.net/js/jquery.dataTables.min.js')!!}"></script>
<script src="{!! asset('master/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')!!}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{!! asset('master/bower_components/bootstrap/dist/js/bootstrap.min.js')!!}"></script>
<!-- Morris.js charts -->
<script src="{!! asset('master/bower_components/raphael/raphael.min.js')!!}"></script>
<script src="{!! asset('master/bower_components/morris.js/morris.min.js')!!}"></script>
<!-- Sparkline -->
<script src="{!! asset('master/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')!!}"></script>
<!-- jvectormap -->
<script src="{!! asset('master/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')!!}"></script>
<script src="{!! asset('master/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')!!}"></script>
<!-- jQuery Knob Chart -->
<script src="{!! asset('master/bower_components/jquery-knob/dist/jquery.knob.min.js')!!}"></script>
<!-- daterangepicker -->
<script src="{!! asset('master/bower_components/moment/min/moment.min.js')!!}"></script>
<script src="{!! asset('master/bower_components/bootstrap-daterangepicker/daterangepicker.js')!!}"></script>
<!-- datepicker -->
<script src="{!! asset('master/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')!!}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{!! asset('master/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')!!}"></script>

<!-- Slimscroll -->
<script src="{!! asset('master/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')!!}"></script>
<!-- FastClick -->
<script src="{!! asset('master/bower_components/fastclick/lib/fastclick.js')!!}"></script>
<!-- AdminLTE App -->
<script src="{!! asset('master/dist/js/adminlte.min.js')!!}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{!! asset('master/dist/js/pages/dashboard.js')!!}"></script>
<script src="{!! asset('master/dist/js/pages/dashboard2.js') !!}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{!! asset('master/dist/js/demo.js')!!}"></script>
</body>
</html>

<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
        })
    })
</script>