@extends('layouts.master')
@section('title')
    Sponsor | Details
@endsection
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Details of Sponsor {{$sponsorDetails->firstname}} {{$sponsorDetails->lastname}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Details of a sponsor</li>
        </ol>
    </section>

    <!-- Main content -->
    <div class="box">
        <div class="box-body">
            <div class="col-md-6 col-md-offset-3">
                <form action="{{route('sponsors.destroy',$sponsorDetails->id)}}" id="delete-form-{{ $sponsorDetails->id }}" method="post">
                    {{csrf_field()}}
                    {!! method_field('DELETE') !!}
                    <div class="form-group row">
                        <label for="firstname" class="col-sm-3 col-form-label">First Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="firstname" name="firstname" value="{!!$sponsorDetails->firstname!!}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lastname" class="col-sm-3 col-form-label">Last Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="lastname" name="lastname" value="{!!$sponsorDetails->lastname!!}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="idnumber" class="col-sm-3 col-form-label">ID Number</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="idnumber" name="idnumber" value="{!!$sponsorDetails->idnumber!!}" readonly>
                        </div>
                    </div>
                    <fieldset class="form-group">
                        <div class="row">
                            <label for="gender" class="col-sm-3 col-form-label">Gender</label>
                            <div class="col-sm-9">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gender" id="gender"
                                           value="Male"  {{ $sponsorDetails->gender == 'Male' ? 'checked' : '' }} readonly>
                                    <label class="form-check-label" for="gender" >
                                        Male
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gender" id="gender"
                                           value="Female"  {{ $sponsorDetails->gender  == 'Female' ? 'checked' : '' }} readonly>
                                    <label class="form-check-label" for="gender">
                                        Female
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group row">
                        <label for="contact" class="col-sm-3 col-form-label">Contact</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="contact" name="contact" value="{!!$sponsorDetails->contact!!}" readonly>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="residence" class="col-sm-3 col-form-label">Residence</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="residence" name="residence" value="{!!$sponsorDetails->residence!!}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="occupation" class="col-sm-3 col-form-label">Residence</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="occupation" name="occupation" value="{!!$sponsorDetails->occupation!!}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-4 col-sm-offset-4">
                            @if(auth()->user()->can('edit-sponsor'))
                            <a href="{!! route('sponsors.edit',$sponsorDetails->id) !!}"><button type="button" class="btn btn-warning btn-md">Edit</button></a>
                            @endif
                        </div>
                        <div class="col-sm-4">
                            @if(auth()->user()->can('delete-sponsor'))
                            <button type="button" class="btn btn-danger btn-md" data-toggle="modal" data-target="#modal-danger-{{$sponsorDetails->id}}">Delete</button>
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal modal-danger fade" id="modal-danger-{{$sponsorDetails->id}}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Delete?</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to permanently delete {!! $sponsorDetails->firstname !!} {!! $sponsorDetails->lastname !!}?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal" onclick="event.preventDefault();">No</button>
                        <button type="button" class="btn btn-outline" onclick="
                                document.getElementById('delete-form-{{ $sponsorDetails->id }}').submit();
                                ">Yes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
            <form id="delete-form-{{ $sponsorDetails->id }}" method="post"
                  action="{{ route('sponsors.destroy',$sponsorDetails->id) }}" style="display: none">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
            </form>
        </div>

    </div>
    <!-- /.content -->
@endsection