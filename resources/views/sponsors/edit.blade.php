@extends('layouts.master')
@section('title')
    Sponsor | Edit
@endsection
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
           Edit the details of {{$editSponsor->firstname}} {{$editSponsor->lastname}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Edit the details of a sponsor</li>
        </ol>
    </section>

    <!-- Main content -->
    <div class="box">
        <div class="box-body">
            <div class="col-md-6 col-md-offset-3">
                <form action="{{route('sponsors.update',$editSponsor->id)}}" method="post">
                    {{csrf_field()}}
                    {!! method_field('PUT') !!}
                    <div class="form-group row">
                        <label for="firstname" class="col-sm-3 col-form-label">First Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="firstname" name="firstname" value="{!!$editSponsor->firstname!!}" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lastname" class="col-sm-3 col-form-label">Last Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="lastname" name="lastname" value="{!!$editSponsor->lastname!!}" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="idnumber" class="col-sm-3 col-form-label">ID Number</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="idnumber" name="idnumber" value="{!!$editSponsor->idnumber!!}">
                        </div>
                    </div>
                    <fieldset class="form-group">
                        <div class="row">
                            <label for="gender" class="col-sm-3 col-form-label">Gender</label>
                            <div class="col-sm-9">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gender" id="gender"
                                           value="Male"  {{ $editSponsor->gender == 'Male' ? 'checked' : '' }} >
                                    <label class="form-check-label" for="gender" >
                                        Male
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gender" id="gender"
                                           value="Female"  {{ $editSponsor->gender  == 'Female' ? 'checked' : '' }} >
                                    <label class="form-check-label" for="gender">
                                        Female
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group row">
                        <label for="contact" class="col-sm-3 col-form-label">Contact</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="contact" name="contact" value="{!!$editSponsor->contact!!}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="residence" class="col-sm-3 col-form-label">Residence</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="residence" name="residence" value="{!!$editSponsor->residence!!}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="occupation" class="col-sm-3 col-form-label">Residence</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="occupation" name="occupation" value="{!!$editSponsor->occupation!!}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 col-md-offset-6"><button type="submit" class="btn btn-primary btn-md">Save Details</button></div>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <!-- /.content -->
@endsection