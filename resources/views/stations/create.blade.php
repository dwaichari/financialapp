@extends('layouts.master')
@section('title')
    Station | Add
@endsection
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add a new station
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add a new station to the database</li>
        </ol>
    </section>

    <div class="box box-info">
        <!-- form start -->
        <form class="form-horizontal" action="{!! route('stations.store') !!}" method="post">
            {!! csrf_field() !!}
            <div class="box-body">
                <div class="col-md-offset-2">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" name="name" placeholder="Enter the name of the station" >
                            @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-2">
                    <div class="form-group">
                        <label for="location" class="col-sm-2 control-label">Location</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control {{ $errors->has('location') ? ' is-invalid' : '' }}" id="location" name="location" placeholder="Enter the location of the station" >
                            @if ($errors->has('location'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>

                    <div class="col-sm-2 col-md-offset-4">
                        <button type="submit" class="btn btn-block btn-primary">Save</button>
            </div>
            </div>
        </form>
    </div>
    <!-- /.content-wrapper -->
    <!-- /.content -->
@endsection