@extends('layouts.master')
@section('title')
    Stations | List
@endsection
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Stations List
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">This shows all the stations available in the database/li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12 col-xs-6">
                <!-- /.box -->

                <div class="box">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th>Name</th>
                                <th>Location</th>
                                <th>Date Created</th>
                                <th class="text-center">Action</th>
                                <th class="text-center">Manage</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($stations as $station)
                            <tr>
                                <td class="text-center">{{ $loop->index + 1 }}</td>
                                <td>{!! $station->name !!}</td>
                                <td>{!! $station->location !!}</td>
                                <td>{!! $station->created_at !!}</td>
                                <td class="text-center"> @if(auth()->user()->can('edit-station'))
                                    <a href="{{ route('stations.edit',$station->id) }}"><span class="glyphicon glyphicon-edit" style="margin-right: 8px;"></span></a>
                                    @endif
                                    @if(auth()->user()->can('delete-station'))
                                        <a data-toggle="modal" data-target="#modal-danger-{{$station->id}}"><span class="glyphicon glyphicon-trash "></span></a>
                                    @endif
                                        <div class="modal modal-danger fade" id="modal-danger-{{$station->id}}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Delete?</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure you want to permanently delete {!! $station->name !!} station?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal" onclick="event.preventDefault();">No</button>
                                                    <button type="button" class="btn btn-outline" onclick="
                                                            document.getElementById('delete-form-{{ $station->id }}').submit();
                                                            ">Yes</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                        <form id="delete-form-{{ $station->id }}" method="post"
                                              action="{{ route('stations.destroy',$station->id) }}" style="display: none">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                        </form>
                                    </div>
                                </td>
                                <td><div class="col-sm-4">
                                        <a href="{!! route('station.clients',$station->id) !!}" class="btn btn-block btn-info btn-sm">Clients</a>
                                    </div>
                                    <div class="col-sm-4">
                                        <a href="{!! route('station.balances',$station->id) !!}" class="btn btn-block btn-info btn-sm">Balances</a>
                                    </div>
                                    <div class="col-sm-4">
                                        <a href="{!! route('station.payments',$station->id) !!}" class="btn btn-block btn-info btn-sm">Payments</a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th>Name</th>
                                <th>Location</th>
                                <th>Date Created</th>
                                <th class="text-center">Action</th>
                                <th class="text-center">Manage</th>
                            </tr>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
        <!-- /.modal -->

        <div class="modal modal-danger fade" id="modal-danger">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Danger Modal</h4>
                    </div>
                    <div class="modal-body">
                        <p>One fine body&hellip;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-outline">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
    <!-- /.content -->
@endsection