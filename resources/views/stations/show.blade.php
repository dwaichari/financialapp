@extends('layouts.master')
@section('title')
    Station | Show
@endsection
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Details of  <strong>{{$stationDetails->name}}</strong> Station
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Edit the details of a station</li>
        </ol>
    </section>

    <div class="box box-info">
        <!-- form start -->
        <form class="form-horizontal" action="{!! route('stations.update',$stationDetails->id) !!}" id="delete-form-{{ $stationDetails->id }}"method="post">
            {!! csrf_field() !!}
            {!! method_field('DELETE') !!}
            <div class="box-body">
                <div class="col-md-offset-2">
                    <div class="form-group">
                        <label for="name" class="col-sm-2 control-label">Name</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name" name="name" value="{!! $stationDetails->name !!}" readonly >
                            @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-2">
                    <div class="form-group">
                        <label for="location" class="col-sm-2 control-label">Location</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control {{ $errors->has('location') ? ' is-invalid' : '' }}" id="location" name="location" value="{!! $stationDetails->location !!}" readonly>
                            @if ($errors->has('location'))
                                <span class="invalid-feedback">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                </div>
                <div class="col-md-offset-2">
                    <div class="form-group">
                        <div class="col-md-2 col-md-offset-2">
                            @if(auth()->user()->can('edit-station') )
                            <a href="{{route('stations.edit',$stationDetails->id)}}"><button type="button" class="btn btn-block btn-warning">Edit</button></a>
                            @endif
                        </div>
                        <div class="col-md-2 col-md-offset-2">
                            @if(auth()->user()->can('delete-station'))
                            <button type="button" class="btn btn-block btn-danger"  data-toggle="modal" data-target="#modal-danger-{{$stationDetails->id}}">Delete</button>
                        </div>
                    </div>
                </div>

            </div>
        </form>
        <div class="modal modal-danger fade" id="modal-danger-{{$stationDetails->id}}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Delete?</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to permanently delete {!! $stationDetails->name !!} station?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal" onclick="event.preventDefault();">No</button>
                        <button type="button" class="btn btn-outline" onclick="
                                document.getElementById('delete-form-{{ $stationDetails->id }}').submit();
                                ">Yes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
            <form id="delete-form-{{ $stationDetails->id }}" method="post"
                  action="{{ route('stations.destroy',$stationDetails->id) }}" style="display: none">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
            </form>
        </div>
    </div>
    <!-- /.content-wrapper -->
    <!-- /.content -->
@endsection