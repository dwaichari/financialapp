@extends('layouts.master')
@section('title')
    Clients | Add
@endsection
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Add Client
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Admit a new client</li>
        </ol>
    </section>

    <!-- Main content -->
    <div class="box">
        <div class="box-body">
            <form action="{{route('clients.store')}}" method="post">
                {{csrf_field()}}
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="sponsor_id" class="col-sm-3 col-form-label" >Sponsor's ID Number</label>
                        <div class="col-sm-9">
                            <select  class="form-control" name="sponsor_id" id="sponsor_id">
                                <option value="">Select the ID Number of the Sponsor</option>
                                @foreach($sponsors as $sponsor)
                                    <option value="{{$sponsor->id}}"  @if (old('sponsor_id') == $sponsor->id) selected="selected" @endif>{!! $sponsor->idnumber !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="station_id" class="col-sm-3 col-form-label">Station</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="station_id" id="station_id">
                                <option value="">Select the station you wish to admit the client</option>
                                @foreach($stations as $station)
                                    <option value="{{$station->id}}" @if (old('station_id') == $station->id) selected="selected" @endif>{!! $station->name !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="firstname" class="col-sm-3 col-form-label">First Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="firstname" name="firstname" placeholder="First Name" value="{{ old('firstname') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lastname" class="col-sm-3 col-form-label">Last Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="lastname" name="lastname" placeholder="Last Name" value="{{ old('lastname') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="idnumber" class="col-sm-3 col-form-label">ID Number</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="idnumber" name="idnumber" placeholder="ID Number" value="{{ old('idnumber') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="dateofbirth" class="col-sm-3 col-form-label">Date of Birth</label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="dateofbirth" name="dateofbirth" value="{{ old('dateofbirth') }}">
                        </div>
                    </div>
                    <fieldset class="form-group">
                        <div class="row">
                            <label for="gender" class="col-sm-3 col-form-label">Gender</label>
                            <div class="col-sm-9">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gender" id="gender"
                                           value="Male"  {{ old('gender')=="Male" ? 'checked='.'"'.'checked'.'"' : '' }}>
                                    <label class="form-check-label" for="gender" >
                                        Male
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gender" id="gender"
                                           value="Female" {{ old('gender')=="Female" ? 'checked='.'"'.'checked'.'"' : '' }}>
                                    <label class="form-check-label" for="gender">
                                        Female
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group row">
                        <label for="contact" class="col-sm-3 col-form-label">Contact</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="contact" name="contact" placeholder="Enter Phone Number" value="{{ old('contact') }}">
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="residence" class="col-sm-3 col-form-label">Residence</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="residence" name="residence" placeholder="Residence" value="{{ old('residence') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="medicalhistory" class="col-sm-3 col-form-label">Medical History</label>
                        <div class="col-sm-9">
                            <textarea type="text" class="form-control" id="medicalhistory" name="medicalhistory" placeholder="Medical History" rows="4" >{{old('medicalhistory') }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="expectedexitdate" class="col-sm-3 col-form-label">Expected Exit </label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="expectedexitdate" name="expectedexitdate" placeholder="Expected date of finishing the rehab" value="{{ old('expectedexitdate') }}" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="exitdate" class="col-sm-3 col-form-label">Exit Date </label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="exitdate" name="exitdate" placeholder="This is the date that a person is discharged" value="{{ old('exitdate') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="exitcomments" class="col-sm-3 col-form-label">Exit Comments</label>
                        <div class="col-sm-9">
                            <textarea type="text" class="form-control" id="exitcomments" name="exitcomments" placeholder="Describe the overall outcome of the whole rehabilitation  process for the client" rows="4" >{{old('exitcomments') }}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-3 pull-right"><button type="submit" class="btn btn-primary btn-md">Add Client</button></div>
                    </div>
                </div>
            </form>
        </div>

    </div>
    <!-- /.content -->
@endsection