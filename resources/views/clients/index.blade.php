@extends('layouts.master')
@section('title')
    Clients | List
@endsection
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Clients List
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">This shows all the clients across all the centers</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12 col-xs-6">
                <!-- /.box -->

                <div class="box">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th>Adm No</th>
                                <th>First Name</th>
                                <th>First Name</th>
                                <th>ID Number </th>
                                <th>Gender </th>
                                <th>Residence </th>
                                <th>Contact</th>
                                <th>Date Added</th>
                                <th>Action</th>
                                <th>Sponsor</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($clients as $client)
                                <tr>
                                    <td class="text-center">{{ $loop->index + 1 }}</td>
                                    <td>{{$client->id}}</td>
                                    <td><a href="{{route('clients.show',$client->id)}}">{!! $client->firstname !!}</a></td>
                                    <td><a href="{{route('clients.show',$client->id)}}">{!! $client->lastname !!}</a></td>
                                    <td>{!! $client->idnumber !!}</td>
                                    <td>{!! $client->gender !!}</td>
                                    <td>{!! $client->residence !!}</td>
                                    <td>{!! $client->contact !!}</td>
                                    <td>{!! $client->created_at !!}</td>
                                    <td> @if(auth()->user()->can('edit-client'))
                                        <a href="{{ route('clients.edit',$client->id) }}"><span class="glyphicon glyphicon-edit" style="margin-right: 8px;" ></span></a>
                                        @endif
                                        @if(auth()->user()->can('delete-client'))
                                        <a data-toggle="modal" data-target="#modal-danger-{{$client->id}}"><span class="glyphicon glyphicon-trash "></span></a>
                                        @endif
                                            <div class="modal modal-danger fade" id="modal-danger-{{$client->id}}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Delete?</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure you want to permanently delete this set of payment?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal" onclick="event.preventDefault();">No</button>
                                                        <button type="button" class="btn btn-outline" onclick="
                                                                document.getElementById('delete-form-{{ $client->id }}').submit();
                                                                ">Yes</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                            <form id="delete-form-{{ $client->id }}" method="post"
                                                  action="{{ route('clients.destroy',$client->id) }}" style="display: none">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                            </form>
                                        </div>
                                    </td>
                                    <td>
                                            <a href="{!! route('sponsors.show',str_replace(array('[',']','"'),'', $client ->sponsor()->pluck('id'))) !!}" class="btn btn-block btn-info btn-sm">Sponsor</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th>Adm No</th>
                                <th>First Name</th>
                                <th>ID Number </th>
                                <th>Gender </th>
                                <th>Residence </th>
                                <th>Contact</th>
                                <th>Date Added</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection