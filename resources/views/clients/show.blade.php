@extends('layouts.master')
@section('title')
    Clients | Details
@endsection
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Details of <strong>{{$clientDetails->firstname}} {{$clientDetails->lastname}}</strong>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Edit the details of the client</li>
        </ol>
    </section>

    <!-- Main content -->
    <div class="box">
        <div class="box-body">
            <form action="{{route('clients.destroy',$clientDetails->id)}}" method="post" id="delete-form-{{ $clientDetails->id }}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <div class="col-md-6">
                <div class="form-group row">
                    <label for="sponsor_id" class="col-sm-3 col-form-label">Sponsor</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="sponsor_id" name="sponsor_id"  value="{{str_replace(array('[',']','"'),'', $clientDetails->sponsor()->pluck('idnumber'))}}" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="station_id" class="col-sm-3 col-form-label">Station</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="station_id" name="station_id"  value="{{str_replace(array('[',']','"'),'', $clientDetails->station()->pluck('name'))}}" readonly>
                    </div>
                </div>
                    <div class="form-group row">
                        <label for="firstname" class="col-sm-3 col-form-label">First Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="firstname" name="firstname"  value="{{$clientDetails->firstname}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="lastname" class="col-sm-3 col-form-label">Last Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="lastname" name="lastname"  value="{{$clientDetails->lastname}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="idnumber" class="col-sm-3 col-form-label">ID Number</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="idnumber" name="idnumber"  value="{{$clientDetails->idnumber}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="dateofbirth" class="col-sm-3 col-form-label">Date of Birth</label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="dateofbirth" name="dateofbirth"  value="{{$clientDetails->dateofbirth}}" readonly>
                        </div>
                    </div>
                    <fieldset class="form-group">
                        <div class="row">
                            <label for="gender" class="col-sm-3 col-form-label">Gender</label>
                            <div class="col-sm-9">
                                <div class="form-check">

                                    <input class="form-check-input" type="radio" name="gender" id="gender"
                                           value="Male"  {{ $clientDetails->gender == 'Male' ? 'checked' : '' }} readonly>
                                    <label class="form-check-label" for="gender" >
                                        Male
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="gender" id="gender"
                                           value="Female"  {{ $clientDetails->gender  == 'Female' ? 'checked' : '' }} readonly>
                                    <label class="form-check-label" for="gender">
                                        Female
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group row">
                        <label for="contact" class="col-sm-3 col-form-label">Contact</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="contact" name="contact"  value="{{$clientDetails->contact}}" readonly>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="residence" class="col-sm-3 col-form-label">Residence</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="residence" name="residence" value="{{$clientDetails->residence}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="medicalhistory" class="col-sm-3 col-form-label">Medical History</label>
                        <div class="col-sm-9">
                            <textarea type="text" class="form-control" id="medicalhistory" name="medicalhistory" rows="4" readonly>{{$clientDetails->medicalhistory}}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="expectedexitdate" class="col-sm-3 col-form-label">Expected Exit </label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="expectedexitdate" name="expectedexitdate"  value="{{$clientDetails->expectedexitdate}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="exitdate" class="col-sm-3 col-form-label">Exit Date </label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" id="exitdate" name="exitdate"  value="{{$clientDetails->exitdate}}" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="exitcomments" class="col-sm-3 col-form-label">Exit Comments</label>
                        <div class="col-sm-9">
                            <textarea type="text" class="form-control" id="exitcomments" name="exitcomments"  rows="4" readonly>{{$clientDetails->exitcomments}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    @if(auth()->user()->can('edit-client'))
                    <div class="col-sm-3 col-sm-offset-3"><a href="{{route('clients.edit',$clientDetails->id)}}"><button type="button" class="btn btn-warning btn-md">Edit Details</button></a></div>
                    @endif
                    @if(auth()->user()->can('delete-client'))
                    <div class="col-sm-3  col-sm-offset-3"><button type="button" class="btn btn-danger btn-md" data-toggle="modal" data-target="#modal-danger-{{$clientDetails->id}}">Delete Client</button></div>
                    @endif
                </div>
            </form>
        </div>
        <div class="modal modal-danger fade" id="modal-danger-{{$clientDetails->id}}">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Delete?</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure you want to permanently delete {!! $clientDetails->firstname !!} {!! $clientDetails->lastname !!}?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal" onclick="event.preventDefault();">No</button>
                        <button type="button" class="btn btn-outline" onclick="
                                document.getElementById('delete-form-{{ $clientDetails->id }}').submit();
                                ">Yes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
            <form id="delete-form-{{ $clientDetails->id }}" method="post"
                  action="{{ route('clients.destroy',$clientDetails->id) }}" style="display: none">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
            </form>
        </div>
    </div>
    <!-- /.content -->
@endsection