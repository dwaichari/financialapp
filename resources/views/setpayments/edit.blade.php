@extends('layouts.master')
@section('title')
    Set Payment | Edit
@endsection
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Edit Set Payment
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Edit a set of payment for a certain client</li>
        </ol>
    </section>

    <!-- Main content -->
    <!-- Horizontal Form -->
    <div class="box box-info">
        <!-- form start -->
        <form class="form-horizontal" action="{!! route('setpayments.update',$editsetpayment->id) !!}" method="post">
            {!! csrf_field() !!}
            {{method_field('PUT')}}
            <div class="box-body">
                <div class="col-md-offset-2">
                    <div class="form-group">
                        <label for="client_id" class="col-sm-2 control-label">Client ID</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="client_id" name="client_id" value="{{$editsetpayment->client_id}}" @if (old('client_id') == $client->id) selected="selected" @endif>
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-2">
                    <div class="form-group">
                        <label for="tuitionfee" class="col-sm-2 control-label">Rehab Fee</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="tuitionfee" name="tuitionfee" value="{{$editsetpayment->tuitionfee}}">
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-2">
                    <div class="form-group">
                        <label for="nursefee" class="col-sm-2 control-label">Nurse Fee</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="nursefee" name="nursefee" value="{{$editsetpayment->nursefee}}" >
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-2">
                    <div class="form-group">
                        <label for="psychiatristfee" class="col-sm-2 control-label">Psychiatrist Fee</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="psychiatristfee" name="psychiatristfee" value="{{$editsetpayment->pychiatristfee}}" >
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-2">
                    <div class="form-group">
                        <label for="medicalfee" class="col-sm-2 control-label">Medical Fee</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="medicalfee" name="medicalfee" value="{{$editsetpayment->medicalfee}}" >
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-2">
                    <div class="form-group">
                        <label for="screeningfee" class="col-sm-2 control-label">Screening Fee</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="screeningfee" name="screeningfee" value="{{$editsetpayment->screeningfee}}" >
                        </div>
                    </div>
                    <div class="col-sm-2 col-md-offset-4">
                        <button type="submit" class="btn btn-block btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- /.content-wrapper -->
    <!-- /.content -->
@endsection