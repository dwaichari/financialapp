@extends('layouts.master')
@section('title')
    Set of Payments| Today
@endsection
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Set of Payments today the {{$currentdate}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">This shows all the charges that the clients were charged today {{$currentdate}}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12 col-xs-6">
                <!-- /.box -->

                <div class="box">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th>Adm No</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Rehab </th>
                                <th>Nurse </th>
                                <th>Psychiatric </th>
                                <th>Medical</th>
                                <th>Screening</th>
                                <th>Date Set</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($todayssetpayments as $todayssetpayment)
                            <tr>
                                <td class="text-center">{{ $loop->index + 1 }}</td>
                                <td>{{str_replace(array('[',']','"'),'', $todayssetpayment ->client()->pluck('id'))}}</td>
                                <td><a href="{!! route('clients.show',str_replace(array('[',']','"'),'', $todayssetpayment ->client()->pluck('id'))) !!}">{{str_replace(array('[',']','"'),'', $todayssetpayment ->client()->pluck('firstname'))}}</a></td>
                                <td><a href="{!! route('clients.show',str_replace(array('[',']','"'),'', $todayssetpayment ->client()->pluck('id'))) !!}">{{str_replace(array('[',']','"'),'', $todayssetpayment ->client()->pluck('lastname'))}}</a></td>
                                <td>{!! $todayssetpayment->tuitionfee !!}</td>
                                <td>{!! $todayssetpayment->nursefee !!}</td>
                                <td>{!! $todayssetpayment->psychiatristfee !!}</td>
                                <td>{!! $todayssetpayment->medicalfee !!}</td>
                                <td>{!! $todayssetpayment->screeningfee !!}</td>
                                <td>{!! $todayssetpayment->created_at !!}</td>
                                <td><a href="{{ route('setpayments.edit',$todayssetpayment->id) }}"><span class="glyphicon glyphicon-edit" style="margin-right: 8px;"></span></a>
                                    <a data-toggle="modal" data-target="#modal-danger-{{$todayssetpayment->id}}"><span class="glyphicon glyphicon-trash "></span></a>
                                    <div class="modal modal-danger fade" id="modal-danger-{{$todayssetpayment->id}}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title">Delete?</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Are you sure you want to permanently delete this set of payment?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal" onclick="event.preventDefault();">No</button>
                                                    <button type="button" class="btn btn-outline" onclick="
                                                            document.getElementById('delete-form-{{ $todayssetpayment->id }}').submit();
                                                            ">Yes</button>
                                                </div>
                                            </div>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                        <form id="delete-form-{{ $todayssetpayment->id }}" method="post"
                                              action="{{ route('setpayments.destroy',$todayssetpayment->id) }}" style="display: none">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                        </form>
                                    </div>
                                </td>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th>Adm No</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Rehab </th>
                                <th>Nurse </th>
                                <th>Psychiatric </th>
                                <th>Medical</th>
                                <th>Screening</th>
                                <th>Date Set</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
        <!-- /.modal -->

        <div class="modal modal-danger fade" id="modal-danger">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Danger Modal</h4>
                    </div>
                    <div class="modal-body">
                        <p>One fine body&hellip;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-outline">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
    <!-- /.content -->
@endsection