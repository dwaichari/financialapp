@extends('layouts.master')
@section('title')
    Clients | Payment History
@endsection
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Payment history for <strong>{{$clientdetails->firstname}} {{$clientdetails->lastname}} Adm NO {{$clientdetails->id}}</strong>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">This shows all the payments that a client has ever made</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12 col-xs-6">
                <!-- /.box -->

                <div class="box">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th class="text-center">Rehab</th>
                                <th class="text-center">Psychiatrist</th>
                                <th class="text-center">Nurse</th>
                                <th class="text-center">Medical</th>
                                <th class="text-center">Screening</th>
                                <th class="text-center">Date Paid</th>
                                <th class="text-center">Totals</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($paymenthistories as $paymenthistory)
                                <tr>
                                    <td class="text-center">{{ $loop->index + 1 }}</td>
                                    <td>{{$paymenthistory->tuitionfee}}</td>
                                    <td>{{$paymenthistory->nursefee}}</td>
                                    <td>{{$paymenthistory->psychiatristfee}}</td>
                                    <td>{{$paymenthistory->medicalfee}}</td>
                                    <td>{{$paymenthistory->screeningfee}}</td>
                                    <td>{{$paymenthistory->created_at}}</td>
                                    <td>Total</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th class="text-center">Rehab</th>
                                <th class="text-center">Psychiatrist</th>
                                <th class="text-center">Nurse</th>
                                <th class="text-center">Medical</th>
                                <th class="text-center">Screening</th>
                                <th class="text-center">Date Paid</th>
                                <th class="text-center">Totals</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection