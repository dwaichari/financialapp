@extends('layouts.master')
@section('title')
    Station | Clientss
@endsection
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            {!! $stationDetails->name !!} Station Clients
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Clients available in the {!! $stationDetails->name !!} station</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12 col-xs-6">
                <!-- /.box -->

                <div class="box">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>D.O.B</th>
                                <th>ID Number</th>
                                <th>Gender</th>
                                <th>Residence</th>
                                <th>Contact</th>
                                <th>Date Added</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($stationClients as $stationClient)
                                <tr>
                                    <td class="text-center">{{ $loop->index + 1 }}</td>
                                    <td><a href="{{route('clients.show',$stationClient->id)}}">{!! $stationClient->firstname !!}</a></td>
                                    <td><a href="{{route('clients.show',$stationClient->id)}}">{!! $stationClient->lastname !!}</a></td>
                                    <td>{!! $stationClient->dateofbirth !!}</td>
                                    <td>{!! $stationClient->idnumber !!}</td>
                                    <td>{!! $stationClient->gender !!}</td>
                                    <td>{!! $stationClient->residence !!}</td>
                                    <td>{!! $stationClient->contact !!}</td>
                                    <td>{!! $stationClient->created_at !!}</td>
                                    <td><a href="{{ route('clients.edit',$stationClient->id) }}"><span class="glyphicon glyphicon-edit" style="margin-right: 8px;"></span></a>
                                        <a data-toggle="modal" data-target="#modal-danger-{{$stationClient->id}}"><span class="glyphicon glyphicon-trash "></span></a>
                                        <div class="modal modal-danger fade" id="modal-danger-{{$stationClient->id}}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Delete?</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure you want to permanently delete {!! $stationClient->firstname !!} {!! $stationClient->lastname !!}?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal" onclick="event.preventDefault();">No</button>
                                                        <button type="button" class="btn btn-outline" onclick="
                                                                document.getElementById('delete-form-{{ $stationClient->id }}').submit();
                                                                ">Yes</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                            <form id="delete-form-{{ $stationClient->id }}" method="post"
                                                  action="{{ route('clients.destroy',$stationClient->id) }}" style="display: none">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                            </form>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>D.O.B</th>
                                <th>ID Number</th>
                                <th>Gender</th>
                                <th>Residence</th>
                                <th>Contact</th>
                                <th>Date Added</th>
                                <th>Action</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
        <!-- /.modal -->

        <div class="modal modal-danger fade" id="modal-danger">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Danger Modal</h4>
                    </div>
                    <div class="modal-body">
                        <p>One fine body&hellip;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-outline">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
    <!-- /.content -->
@endsection