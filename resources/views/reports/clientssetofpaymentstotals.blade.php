@extends('layouts.master')
@section('title')
    Clients | Set of Payments
@endsection
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Clients Total Amount of Set of Payments
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">This shows all the clients and their respective amount they are supposed to pay (IN TOTALS)</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12 col-xs-6">
                <!-- /.box -->

                <div class="box">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th class="text-center">Rehab Fees</th>
                                <th class="text-center">Psychiatrist Fees</th>
                                <th class="text-center">Nurse Fees</th>
                                <th class="text-center">Medical Fees</th>
                                <th class="text-center">Screening Fees</th>
                                <th class="text-center">Totals</th>
                                <th class="text-center">History</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($clientssetofpayments as $clientssetofpayment)
                                <tr>
                                    <td class="text-center">{{ $loop->index + 1 }}</td>
                                    <td>{!! $clientssetofpayment->firstname !!}</td>
                                    <td >{!! $clientssetofpayment->lastname !!}</td>
                                    <td class="text-center">{{ str_replace(array('[',']','"'),'', $clientssetofpayment->setpayments()->sum('tuitionfee')) }}</td>
                                    <td class="text-center">{{ str_replace(array('[',']','"'),'', $clientssetofpayment->setpayments()->sum('psychiatristfee')) }}</td>
                                    <td class="text-center">{{ str_replace(array('[',']','"'),'', $clientssetofpayment->setpayments()->sum('nursefee')) }}</td>
                                    <td class="text-center">{{ str_replace(array('[',']','"'),'', $clientssetofpayment->setpayments()->sum('medicalfee')) }}</td>
                                    <td class="text-center">{{ str_replace(array('[',']','"'),'', $clientssetofpayment->setpayments()->sum('screeningfee')) }}</td>
                                    <td class="text-center"> {{ str_replace(array('[',']','"'),'', $clientssetofpayment->setpayments()->sum('tuitionfee')
                                    +$clientssetofpayment->setpayments()->sum('psychiatristfee')
                                     +$clientssetofpayment->setpayments()->sum('nursefee')
                                      +$clientssetofpayment->setpayments()->sum('medicalfee')
                                       +$clientssetofpayment->setpayments()->sum('screeningfee') ) }} </td>
                                    <td>
                                        <a href="{!! route('setofpaymentshistory',$clientssetofpayment->id)!!}" class="btn btn-block btn-info btn-sm">History</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Rehab Fees</th>
                                <th>Psychiatrist Fees</th>
                                <th>Nurse Fees</th>
                                <th>Medical Fees</th>
                                <th>Screening Fees</th>
                                <th>Totals</th>
                                <th class="text-center">History</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection