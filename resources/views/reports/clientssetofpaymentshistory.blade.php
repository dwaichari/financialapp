@extends('layouts.master')
@section('title')
    Clients | Set of Payments History
@endsection
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            History of set of payments for <strong>{{$client->firstname}} {{$client->lastname}} Adm NO {{$client->id}}</strong>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">This shows all the set of payments that a client has ever been charged</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12 col-xs-6">
                <!-- /.box -->

                <div class="box">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th class="text-center">Rehab</th>
                                <th class="text-center">Psychiatrist</th>
                                <th class="text-center">Nurse</th>
                                <th class="text-center">Medical</th>
                                <th class="text-center">Screening</th>
                                <th class="text-center">Date Set</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($setofpaymenthistories as $setofpaymenthistorie)
                                <tr>
                                    <td class="text-center">{{ $loop->index + 1 }}</td>
                                    <td class="text-center">{{$setofpaymenthistorie->tuitionfee}}</td>
                                    <td class="text-center">{{$setofpaymenthistorie->nursefee}}</td>
                                    <td class="text-center">{{$setofpaymenthistorie->psychiatristfee}}</td>
                                    <td class="text-center">{{$setofpaymenthistorie->medicalfee}}</td>
                                    <td class="text-center">{{$setofpaymenthistorie->screeningfee}}</td>
                                    <td class="text-center">{{$setofpaymenthistorie->created_at}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th class="text-center">Rehab</th>
                                <th class="text-center">Psychiatrist</th>
                                <th class="text-center">Nurse</th>
                                <th class="text-center">Medical</th>
                                <th class="text-center">Screening</th>
                                <th class="text-center">Date Set</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection