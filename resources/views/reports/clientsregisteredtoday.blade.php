@extends('layouts.master')
@section('title')
    Clients | Today
@endsection
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Clients registered today {{$currentdate}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Table showing all the clients registered today</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12 col-xs-6">
                <!-- /.box -->

                <div class="box">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th class="text-center">Adm NO</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>ID Number</th>
                                <th>Gender</th>
                                <th>Residence</th>
                                <th>Contact</th>
                                <th>Station</th>
                                <th>Action</th>
                                <th>Manage</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($todaysclients as $todaysclient)
                                <tr>
                                    <td class="text-center">{{ $loop->index + 1 }}</td>
                                    <td class="text-center">{!! $todaysclient->id !!}</td>
                                    <td><a href="{{route('clients.show',$todaysclient->id)}}">{!! $todaysclient->firstname !!}</a></td>
                                    <td><a href="{{route('clients.show',$todaysclient->id)}}">{!! $todaysclient->lastname !!}</a></td>
                                    <td>{!! $todaysclient->idnumber !!}</td>
                                    <td>{!! $todaysclient->gender !!}</td>
                                    <td>{!! $todaysclient->residence !!}</td>
                                    <td>{!! $todaysclient->contact !!}</td>
                                    <td>{!! str_replace(array('[',']','"'),'', $todaysclient ->station()->pluck('name')) !!}</td>
                                    <td><a href="{{ route('clients.edit',$todaysclient->id) }}"><span class="glyphicon glyphicon-edit" style="margin-right: 8px;"></span></a>
                                        <a data-toggle="modal" data-target="#modal-danger-{{$todaysclient->id}}"><span class="glyphicon glyphicon-trash "></span></a>
                                        <div class="modal modal-danger fade" id="modal-danger-{{$todaysclient->id}}">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Delete?</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Are you sure you want to permanently delete {!! $todaysclient->firstname !!} {!! $todaysclient->lastname !!}?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal" onclick="event.preventDefault();">No</button>
                                                        <button type="button" class="btn btn-outline" onclick="
                                                                document.getElementById('delete-form-{{ $todaysclient->id }}').submit();
                                                                ">Yes</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                            <form id="delete-form-{{ $todaysclient->id }}" method="post"
                                                  action="{{ route('clients.destroy',$todaysclient->id) }}" style="display: none">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                            </form>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="{!! route('sponsors.show',str_replace(array('[',']','"'),'', $todaysclient ->sponsor()->pluck('id'))) !!}" class="btn btn-block btn-info btn-sm">Sponsor</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th>First Name</th>
                                <th>Adm NO</th>
                                <th>Last Name</th>
                                <th>ID Number</th>
                                <th>Gender</th>
                                <th>Residence</th>
                                <th>Contact</th>
                                <th>Station</th>
                                <th>Action</th>
                                <th>Manage</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
        <!-- /.modal -->

        <div class="modal modal-danger fade" id="modal-danger">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Danger Modal</h4>
                    </div>
                    <div class="modal-body">
                        <p>One fine body&hellip;</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-outline">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </section>
    <!-- /.content -->
@endsection