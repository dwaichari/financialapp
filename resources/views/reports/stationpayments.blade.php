@extends('layouts.master')
@section('title')
    Station | Payments All
@endsection
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Clients Total Amount of Payments for {{$station->name}} Station
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">This shows all the clients and their respective amount that they have paid (IN TOTALS) for statio {{$station->name}}</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-12 col-xs-6">
                <!-- /.box -->

                <div class="box">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th class="text-center">First Name</th>
                                <th class="text-center">Last Name</th>
                                <th class="text-center">Rehab</th>
                                <th class="text-center">Psychiatrist</th>
                                <th class="text-center">Nurse</th>
                                <th class="text-center">Medical</th>
                                <th class="text-center">Screening</th>
                                <th class="text-center">Totals</th>
                                <th class="text-center">Manage</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($clients as $client)
                                <tr>
                                    <td class="text-center">{{ $loop->index + 1 }}</td>
                                    <td class="text-center"><a href="{!! route('clients.show',$client->id) !!}">{!! $client->firstname !!}</a></td>
                                    <td class="text-center"><a href="{!! route('clients.show',$client->id) !!}">{!! $client->lastname !!}</a></td>
                                    <td class="text-center">{!! $client->payments->sum('tuitionfee') !!}</td>
                                    <td class="text-center">{!! $client->payments->sum('psychiatristfee') !!}</td>
                                    <td class="text-center">{!! $client->payments->sum('nursefee') !!}</td>
                                    <td class="text-center">{!! $client->payments->sum('medicalfee') !!}</td>
                                    <td class="text-center">{!! $client->payments->sum('screeningfee') !!}</td>
                                    <td class="text-center">{!! $client->payments->sum('tuitionfee')
                                    +$client->payments->sum('psychiatristfee')
                                    + $client->payments->sum('nursefee')
                                    +$client->payments->sum('medicalfee')
                                    +$client->payments->sum('screeningfee') !!}</td>
                                    <td class="text-center">
                                        <div class="col-sm-10">
                                            <a href="{!! route('paymenthistory',$client->id)!!}" class="btn btn-block btn-info btn-sm">Payment History</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class="text-center">S/N</th>
                                <th class="text-center">First Name</th>
                                <th class="text-center">Last Name</th>
                                <th class="text-center">Rehab</th>
                                <th class="text-center">Psychiatrist</th>
                                <th class="text-center">Nurse</th>
                                <th class="text-center">Medical</th>
                                <th class="text-center">Screening</th>
                                <th class="text-center">Totals</th>
                                <th class="text-center">Manage</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection