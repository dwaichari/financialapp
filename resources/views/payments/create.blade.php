@extends('layouts.master')
@section('title')
    Payment | Add
@endsection
@section('main-content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pay
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Make a payment for  the client</li>
        </ol>
    </section>

    <div class="box box-info">
        <!-- form start -->
        <form class="form-horizontal" action="{!! route('payments.store') !!}" method="post">
            {!! csrf_field() !!}
            <div class="box-body">
                <div class="col-md-offset-2">
                    <div class="form-group">
                        <label for="client_id" class="col-sm-2 control-label">Client Adm No</label>
                        <div class="col-sm-6">
                            <select required class="form-control" name="client_id" id="client_id">
                                <option >Select the clients admission number</option>
                                @foreach($clients as $client)
                                    <option value="{{$client->id}}" @if (old('client_id') == $client->id) selected="selected" @endif>{!! $client->id !!} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-2">
                    <div class="form-group">
                        <label for="tuitionfee" class="col-sm-2 control-label">Rehab Fee</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="tuitionfee" name="tuitionfee" placeholder="Rehabilitation Fee" >
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-2">
                    <div class="form-group">
                        <label for="nursefee" class="col-sm-2 control-label">Nurse Fee</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="nursefee" name="nursefee" placeholder="Nurse Fee" >
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-2">
                    <div class="form-group">
                        <label for="psychiatristfee" class="col-sm-2 control-label">Psychiatrist Fee</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="psychiatristfee" name="psychiatristfee" placeholder="Psychiatrist Fee" >
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-2">
                    <div class="form-group">
                        <label for="medicalfee" class="col-sm-2 control-label">Medical Fee</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="medicalfee" name="medicalfee" placeholder="Medical Fee" >
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-2">
                    <div class="form-group">
                        <label for="screeningfee" class="col-sm-2 control-label">Screening Fee</label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" id="screeningfee" name="screeningfee" placeholder="Screening Fee" >
                        </div>
                    </div>
                    <div class="col-sm-2 col-md-offset-4">
                        <button type="submit" class="btn btn-block btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- /.content-wrapper -->
    <!-- /.content -->
@endsection