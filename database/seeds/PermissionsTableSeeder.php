<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'administer roles & permissions';
        $permission->save();

        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'show-clients';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'show-client';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'add-client';
        $permission->save();

        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'edit-client';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'delete-client';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'show-sponsors';
        $permission->save();

        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'show-sponsor';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'add-sponsor';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'edit-sponsor';
        $permission->save();

        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'delete-sponsor';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'show-setofpayments';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'show-setofpayment';
        $permission->save();

        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'edit-setofpayment';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'delete-setofpayment';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'show-payments';
        $permission->save();

        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'show-payment';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'add-payment';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'edit-payment';
        $permission->save();

        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'delete-payment';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'add-sponsor';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'add-setofpayment';
        $permission->save();

        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'show-stations';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'show-station';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'add-station';
        $permission->save();

        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'edit-station';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'delete-station';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'show-reports';
        $permission->save();
        $permission = new \Spatie\Permission\Models\Permission();
        $permission->name = 'show-dashboard';
        $permission->save();
    }
}
