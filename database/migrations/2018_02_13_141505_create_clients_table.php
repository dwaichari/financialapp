<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sponsor_id');
            $table->integer('station_id');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('idnumber')->nullable();
            $table->string('dateofbirth');
            $table->string('gender');
            $table->string('contact')->nullable();
            $table->string('residence');
            $table->text('medicalhistory')->nullable();
            $table->date('expectedexitdate')->nullable();
            $table->date('exitdate')->nullable();
            $table->text('exitcomments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
